package net.nilosplace.goosecoveapi;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class App {
	
	ObjectMapper objectMapper = new ObjectMapper();
	
	// Reports
	// Freq of over 100 mph
	//

	public App() {

		List<Speed> speeds = readData();

		int maxSpeed = 0;
		for (Speed speed : speeds) {
			Integer testSpeed = Integer.parseInt(speed.getSpeed());
			if (testSpeed > maxSpeed) {
				maxSpeed = testSpeed;
			}
			if(testSpeed >= 100) {
				System.out.println(speed);
			}
		}

		HashMap<String, HashMap<String, HashMap<String, Integer>>> dataMap = generateMaps(speeds);
		
		for (String key : dataMap.keySet()) {
			YearData data = generateHeatMap(dataMap.get(key), key, maxSpeed);

			try {
				objectMapper.writeValue(new File("/Users/olinblodgett/git/goosecove/src/" + key + ".json"), data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		
		
	}

	private YearData generateHeatMap(HashMap<String, HashMap<String, Integer>> yearMap, String year, Integer maxSpeed) {

		System.out.println(yearMap.keySet().size());
		
		List<String> dayKeys = new ArrayList<>();
		dayKeys.addAll(yearMap.keySet());
		Collections.sort(dayKeys);
		
		List<String> x = new ArrayList<>();
		List<String> y = new ArrayList<>();
		List<List<Integer>> z = new ArrayList<>();
		
		for(int i = 0; i < maxSpeed + 1; i++) {
			y.add(i + "");
			z.add(new ArrayList<>());
		}
		
		for(String day: dayKeys) {
			x.add(day);
			
			for(int i = 10; i < maxSpeed + 1; i++) {
				Integer speedCount = yearMap.get(day).get(i + "");
				if(speedCount == null) speedCount = 0;
				
				z.get(i).add(speedCount);
			}
		}
		
		YearData data = new YearData();
		data.setX(x);
		data.setY(y);
		data.setZ(z);
		
		return data;
		
	}

	private HashMap<String, HashMap<String, HashMap<String, Integer>>> generateMaps(List<Speed> speeds) {
		HashMap<String, HashMap<String, HashMap<String, Integer>>> map = new HashMap<>();

		for (Speed speed : speeds) {
			String array[] = speed.getDate().split("-");
			HashMap<String, HashMap<String, Integer>> yearMap = map.get(array[0]);

			if (yearMap == null) {
				yearMap = new HashMap<String, HashMap<String, Integer>>();
				map.put(array[0], yearMap);
			}

			HashMap<String, Integer> dayMap = yearMap.get(speed.getDate());

			if (dayMap == null) {
				dayMap = new HashMap<String, Integer>();
				yearMap.put(speed.getDate(), dayMap);
			}

			Integer speedCount = dayMap.get(speed.getSpeed());

			if (speedCount == null) {
				speedCount = 0;
			}

			dayMap.put(speed.getSpeed(), speedCount + 1);

		}
		return map;
	}

	public List<Speed> readData() {
		CsvMapper csvMapper = new CsvMapper();
		CsvSchema schema = CsvSchema.emptySchema().withHeader();

		List<Speed> ret = new ArrayList<Speed>();

		ObjectReader oReader = csvMapper.reader(Speed.class).with(schema);
		try (Reader reader = new FileReader("Radar_127GooseCoveWest_Jan2020toJuly2023.csv")) {
			MappingIterator<Speed> mi = oReader.readValues(reader);
			while (mi.hasNext()) {
				Speed speed = mi.next();
				if (speed.getDate() == null || speed.getDate().length() == 0) {
					// System.out.println(speed);
					speed.setDate(speed.getTime());
					speed.setTime("00:00:00");
					// System.out.println(speed);
				}
				ret.add(speed);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static void main(String[] args) {
		new App();
	}
}
