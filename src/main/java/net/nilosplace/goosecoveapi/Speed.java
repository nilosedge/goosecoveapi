package net.nilosplace.goosecoveapi;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class Speed {
	private String date;
	private String time;
	private String speed;
	
	public void setDate(String date) {
		this.date = date.strip();
	}
	public void setTime(String time) {
		this.time = time.strip();
	}
}
