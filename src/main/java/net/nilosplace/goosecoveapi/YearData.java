package net.nilosplace.goosecoveapi;

import java.util.List;

import lombok.Data;

@Data
public class YearData {
	private List<String> x;
	private List<String> y;
	private List<List<Integer>> z;
	private String type = "heatmap";
	private Boolean hoverongaps = false;
}
